package Wspolne;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import Metody.Parametry;
import Metody.Podstawowe_metody;



public class Logowanie {
	 private WebDriver driver;
	 private String baseUrl;
	 Podstawowe_metody podstawowe_metody;
	 WebDriverWait wait;
	 
	
@Before
 public void setUp() throws Exception {
	Parametry parametry = new Parametry();
	driver = parametry.getDriver();
	baseUrl = parametry.getAdres_url();
	podstawowe_metody = new Podstawowe_metody(driver);
 }

@Test
 public void test() throws InterruptedException {
	driver.get(baseUrl);
	podstawowe_metody.logowanie();

}
@After
	public void tearDown() throws Exception {
	podstawowe_metody.wyloguj();
	driver.quit();	
	}

}

