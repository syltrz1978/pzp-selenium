package Metody;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class Zamowienie {
	WebDriver driver;
	Mapowanie_xpath mapowanie = new Mapowanie_xpath();
	Podstawowe_metody podstawowe_metody;
	private HashMap<String, String> pozycje_map;


		public Zamowienie(WebDriver driver) {
		super();
		this.driver = driver;
		pozycje_map =new HashMap<String, String>();
		
	}
			//przejście na plan zamówień
			public void plan_zamowienien() throws InterruptedException {
			podstawowe_metody = new Podstawowe_metody(driver);
			
				//Zamówienia publiczne
				podstawowe_metody.kliknij(mapowanie.getZamowienie_pub());
				//Plan zamówień 
				podstawowe_metody.kliknij(mapowanie.getPlan_zamowien());
			}
			
			//weryfikacja wartosci euro na zamówieniu
			public void zamowienie_weryfikacja_euro() {
				System.out.println(getPozycje_map().get("kurs_e"));
				System.out.println(getPozycje_map().get("wartosc"));
				System.out.println(driver.findElement(By.xpath(mapowanie.getWartosc_euro())).getAttribute("value"));
				assertEquals(podstawowe_metody.iloraz(getPozycje_map().get("kurs_e"), getPozycje_map().get("wartosc")), driver.findElement(By.xpath(mapowanie.getWartosc_euro())).getAttribute("value").replace(" ", ""));
				
			}
			
			public void zamowienie(String przedmiot, String rodzaj, String kategoria, String cpv, String podstawa, String data_typ, String uzasadnienie, String kurs_e) throws InterruptedException {
				pozycje_map.put("przedmiot", przedmiot);
				pozycje_map.put("rodzaj", rodzaj);
				pozycje_map.put("kategoria", kategoria);
				pozycje_map.put("cpv", cpv);
				pozycje_map.put("podstawa", podstawa);
				pozycje_map.put("data_typ", data_typ);
				pozycje_map.put("uzasadnienie", uzasadnienie);
				pozycje_map.put("wartosc", podstawowe_metody.getDf().format(podstawowe_metody.random(50000)));
				pozycje_map.put("kurs_e", kurs_e);
				
							
				podstawowe_metody = new Podstawowe_metody(driver);
				
				
				//Wypelnienie zamowienia
					//czekaj na przycisk drukuj
				podstawowe_metody.czekajNaObecnosc(By.xpath("/html[1]/body[1]/div[1]/div[2]/div[2]/div[2]/table[1]/tbody[1]/tr[1]/td[1]"));
					
					//przedmiot
				podstawowe_metody.wypelnij(mapowanie.getPrzedmiot(), przedmiot);
				
					//rodzaj
				podstawowe_metody.wypelnij_combo(mapowanie.getRodzaj(), rodzaj);
				
					//kategoria
				podstawowe_metody.wypelnij_combo(mapowanie.getKategoria(), kategoria);
				
					//CPV
				podstawowe_metody.wypelnij_combo(mapowanie.getCpv(), cpv);
				
					//podstawa prawna art.30 ust 4
				podstawowe_metody.wypelnij_combo(mapowanie.getPodstawa_prawna(), podstawa);
				
					//data wszczecia postepowania
				podstawowe_metody.wypelnij_combo(mapowanie.getData(), data_typ);
				
					//data
				if (data_typ == "1") {
					podstawowe_metody.wypelnij(mapowanie.getData_wartosc(), "2021-12-31");					
				}
				else if (data_typ == "2")   {
					podstawowe_metody.wypelnij_combo(mapowanie.getData_miesiac(), "6");
				}
				else {
					podstawowe_metody.wypelnij_combo(mapowanie.getData_kwartal(), "3");
				}
			
					//wartosc PLN
				podstawowe_metody.wypelnij_wartosc(mapowanie.getWartosc(), pozycje_map.get("wartosc"));

					//uzasadnienie
				podstawowe_metody.wypelnij(mapowanie.getUzasadnienie(), uzasadnienie);
			
					//weryfikacja przeliczania euro 
				zamowienie_weryfikacja_euro();
				
					//zapisz 
				podstawowe_metody.kliknij(mapowanie.getOk());
			}

			public HashMap<String, String> getPozycje_map() {
				return pozycje_map;
			}

			public void setPozycje_map(HashMap<String, String> pozycje_map) {
				this.pozycje_map = pozycje_map;
			}
			
			

}
