package Metody;

public class Mapowanie_xpath {

	private String plan_zamowien = "/html[1]/body[1]/div[1]/div[2]/div[2]/div[2]/table[1]/tbody[1]/tr[5]/td[2]";
	private String zbiorcze = "/html[1]/body[1]/div[1]/div[2]/div[2]/div[2]/table[1]/tbody[1]/tr[6]/td[2]";
	
	private String naglowek = "/html[1]/body[1]/div[1]/div[3]/div[1]/h6[1]/div[1]/div[4]/table[1]/thead[1]/tr[1]/td[1]/span[1]"; //trzy kreski w nagłowku 
	private String chekbox_zam = "/html[1]/body[1]/div[1]/div[3]/div[1]/h6[1]/div[1]/div[5]/table[1]/tbody[1]/tr[1]/td[1]/input[1]";
	
	private String dodaj_zam = "/html[1]/body[1]/div[1]/div[2]/div[2]/div[2]/table[1]/tbody[1]/tr[5]/td[2]";
	private String edytuj_zam = "/html[1]/body[1]/div[1]/div[2]/div[2]/div[2]/table[1]/tbody[1]/tr[7]/td[2]";
	
	//dodaj zamówienie
	private String przedmiot = "/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/div[4]/div[1]/div[3]/textarea[1]";
	private String rodzaj = "/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/div[4]/div[1]/div[4]";
	private String kategoria = "/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/div[4]/div[1]/div[5]";
	private String zamowienie_pub = "/html[1]/body[1]/div[1]/div[2]/div[2]/div[2]/table[1]/tbody[1]/tr[4]/td[2]";
	private String wartosc = "/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/div[4]/div[3]/div[5]/input[1]";
	private String cpv = "/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/div[4]/div[1]/div[6]";
	private String podstawa_prawna = "/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/div[4]/div[3]/div[1]";
	
	private String data = "/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/div[4]/div[3]/div[2]"; 
	private String data_wartosc = "/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/div[4]/div[3]/div[3]/div[1]/div[1]/input[1]";
	private String data_miesiac = "/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/div[4]/div[3]/div[3]/div[2]/div[1]";
	private String data_kwartal = "/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/div[4]/div[3]/div[3]/div[3]/div[1]";

	private String uzasadnienie = "/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/div[4]/div[3]/div[4]/textarea[1]";
	private String kurs = "/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/div[4]/div[3]/div[7]/input[1]";
	private String wartosc_euro = "/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/div[4]/div[3]/div[6]/input[1]";
	
	private String ok = "/html[1]/body[1]/div[1]/div[3]/div[1]/div[1]/div[2]";
	
	//zamówienie
	private String nr_zam = "/html[1]/body[1]/div[1]/div[3]/div[1]/h6[1]/div[1]/div[5]/table[1]/tbody[1]/tr[1]/td[7]";
	private String suma_kat = "/html[1]/body[1]/div[1]/div[3]/div[1]/h6[1]/div[1]/div[5]/table[1]/tbody[1]/tr[1]/td[14]";
	
	private String opracowany = "/html[1]/body[1]/div[1]/div[2]/div[2]/div[2]/table[1]/tbody[1]/tr[11]/td[2]";
	private String zatwierdz = "/html[1]/body[1]/div[1]/div[2]/div[2]/div[2]/table[1]/tbody[1]/tr[12]/td[2]";
	private String akceptuj =  "/html[1]/body[1]/div[1]/div[2]/div[2]/div[2]/table[1]/tbody[1]/tr[13]/td[2]";
	
	private String powrot = "/html[1]/body[1]/div[1]/div[2]/div[2]/div[2]/table[1]/tbody[1]/tr[2]/td[2]";
	private String powrot_zbiorczy = "/html[1]/body[1]/div[1]/div[2]/div[2]/div[2]/table[1]/tbody[1]/tr[1]/td[2]";
							
	private String gen_zbiorczy ="/html[1]/body[1]/div[1]/div[2]/div[2]/div[2]/table[1]/tbody[1]/tr[4]/td[2]";



//gettery i settery 
	public String getRodzaj() {
		return rodzaj;
	}
	public void setRodzaj(String rodzaj) {
		this.rodzaj = rodzaj;
	}
	public String getKategoria() {
		return kategoria;
	}
	public void setKategoria(String kategoria) {
		this.kategoria = kategoria;
	}
	public String getZamowienie_pub() {
		return zamowienie_pub;
	}
	public void setZamowienie_pub(String zamowienie_pub) {
		this.zamowienie_pub = zamowienie_pub;
	}
	public String getWartosc() {
		return wartosc;
	}
	public void setWartosc(String wartosc) {
		this.wartosc = wartosc;
	}
	public String getPlan_zamowien() {
		return plan_zamowien;
	}
	public void setPlan_zamowien(String plan_zamowien) {
		this.plan_zamowien = plan_zamowien;
	}
	public String getZbiorcze() {
		return zbiorcze;
	}
	public String getNaglowek() {
		return naglowek;
	}
	public void setNaglowek(String naglowek) {
		this.naglowek = naglowek;
	}
	public String getChekbox_zam() {
		return chekbox_zam;
	}
	public void setChekbox_zam(String chekbox_zam) {
		this.chekbox_zam = chekbox_zam;
	}
	public void setZbiorcze(String zbiorcze) {
		this.zbiorcze = zbiorcze;
	}
	public String getDodaj_zam() {
		return dodaj_zam;
	}
	public void setDodaj_zam(String dodaj_zam) {
		this.dodaj_zam = dodaj_zam;
	}
	public String getPrzedmiot() {
		return przedmiot;
	}
	public void setPrzedmiot(String przedmiot) {
		this.przedmiot = przedmiot;
	}
	public String getCpv() {
		return cpv;
	}
	public void setCpv(String cpv) {
		this.cpv = cpv;
	}
	public String getPodstawa_prawna() {
		return podstawa_prawna;
	}
	public void setPodstawa_prawna(String podstawa_prawna) {
		this.podstawa_prawna = podstawa_prawna;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getData_wartosc() {
		return data_wartosc;
	}
	public void setData_wartosc(String data_wartosc) {
		this.data_wartosc = data_wartosc;
	}
	public String getUzasadnienie() {
		return uzasadnienie;
	}
	public void setUzasadnienie(String uzasadnienie) {
		this.uzasadnienie = uzasadnienie;
	}
	public String getOk() {
		return ok;
	}
	public void setOk(String ok) {
		this.ok = ok;
	}
	public String getNr_zam() {
		return nr_zam;
	}
	public void setNr_zam(String nr_zam) {
		this.nr_zam = nr_zam;
	}
	public String getData_miesiac() {
		return data_miesiac;
	}
	public void setData_miesiac(String data_miesiac) {
		this.data_miesiac = data_miesiac;
	}
	public String getData_kwartal() {
		return data_kwartal;
	}
	public void setData_kwartal(String data_kwartal) {
		this.data_kwartal = data_kwartal;
	}
	public String getEdytuj_zam() {
		return edytuj_zam;
	}
	public void setEdytuj_zam(String edytuj) {
		this.edytuj_zam = edytuj;
	}
	public String getSuma_kat() {
		return suma_kat;
	}
	public void setSuma_kat(String suma_kat) {
		this.suma_kat = suma_kat;
	}
	public String getOpracowany() {
		return opracowany;
	}
	public void setOpracowany(String opracowany) {
		this.opracowany = opracowany;
	}
	public String getZatwierdz() {
		return zatwierdz;
	}
	public void setZatwierdz(String zatwierdz) {
		this.zatwierdz = zatwierdz;
	}
	public String getAkceptuj() {
		return akceptuj;
	}
	public void setAkceptuj(String akceptuj) {
		this.akceptuj = akceptuj;
	}
	public String getPowrot() {
		return powrot;
	}
	public void setPowrot(String powrot) {
		this.powrot = powrot;
	}
	public String getGen_zbiorczy() {
		return gen_zbiorczy;
	}
	public void setGen_zbiorczy(String gen_zbiorczy) {
		this.gen_zbiorczy = gen_zbiorczy;
	}
	public String getPowrot_zbiorczy() {
		return powrot_zbiorczy;
	}
	public void setPowrot_zbiorczy(String powrot_zbiorczy) {
		this.powrot_zbiorczy = powrot_zbiorczy;
	}
	public String getKurs() {
		return kurs;
	}
	public void setKurs(String kurs) {
		this.kurs = kurs;
	}
	public String getWartosc_euro() {
		return wartosc_euro;
	}
	public void setWartosc_euro(String wartosc_euro) {
		this.wartosc_euro = wartosc_euro;
	}


	
}
