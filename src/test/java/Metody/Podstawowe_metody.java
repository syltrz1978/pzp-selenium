package Metody;

import java.text.DecimalFormat;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Podstawowe_metody {

	WebDriver driver;
	WebDriverWait wait;
	DecimalFormat df = new DecimalFormat("###0.00");
	Mapowanie_xpath mapowanie = new Mapowanie_xpath();

	public Podstawowe_metody(WebDriver driver) {
		super();
		this.driver = driver;
		this.wait = new WebDriverWait(driver, 60);
	}

// Logowanie
	public void logowanie() throws InterruptedException {
		this.driver.manage().deleteAllCookies();
		this.driver.navigate().refresh();
		//czeka na pole e-mail
		czekajNaObecnosc(By.xpath("/html[1]/body[1]/div[1]/div[3]/form[1]/div[1]/div[1]/input[1]"));
		//e-mail
		this.driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[3]/form[1]/div[1]/div[1]/input[1]")).clear();
		this.driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[3]/form[1]/div[1]/div[1]/input[1]")).sendKeys("justyna.stanczyk@doskomp.pl");
		//hasło
		this.driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[3]/form[1]/div[1]/div[2]/input[1]")).clear();
		this.driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[3]/form[1]/div[1]/div[2]/input[1]")).sendKeys("Doskomp1");
		//Zaloguj
		this.driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[3]/form[1]/button[1]")).click();
	}

// Wyloguj
		public void wyloguj() throws InterruptedException {
		this.driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[2]/div[1]/span[1]")).click();
		this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html[1]/body[1]/div[1]/div[2]/div[1]/span[1]")));
		}

//Przejdż na plan zamówień 
		public void plan_zamowienien() throws InterruptedException{
			//Zamówienia publiczne
			kliknij(mapowanie.getZamowienie_pub());
			//Plan zamówień 
			kliknij(mapowanie.getPlan_zamowien());
		}
		
		
////Obsługa czkawki
//		public void czekajAzWodaZniknie() throws InterruptedException {
//			By xpath = By.xpath("/html[1]/body[1]/div[4]/img[1]");
////			this.wait.until(ExpectedConditions.presenceOfElementLocated(xpath));
//			Thread.sleep(500);
//
//			while (czyIstnieje(xpath)) {
//				this.wait.until(ExpectedConditions.invisibilityOfElementLocated(xpath));
//				Thread.sleep(500);
//			}
//			Thread.sleep(500);
//		}
		
//Randomowe wartości 
		public double random (int limit) {
		return Math.floor(Math.random() * limit * 100) / 100;
		}		

//Randomowe wartosci - int
		public int random_int (int limit) {
		return (int) Math.floor(Math.random() * limit);
		}	
		
//Wypelnienie wartosciami - kwota
		public void wypelnij_wartosc (String pole, String wartosc) throws InterruptedException{
			for (int i = 0; i<10; i++) {
			driver.findElement(By.xpath(pole)).sendKeys(Keys.BACK_SPACE);
			}
			driver.findElement(By.xpath(pole)).sendKeys(wartosc);
			
		}
//Wypelnienie wartosciami - kwota
		public void wypelnij (String pole, Double wartosc) throws InterruptedException{
			for (int i = 0; i<10; i++) {
				driver.findElement(By.xpath(pole)).sendKeys(Keys.BACK_SPACE);
				}
			driver.findElement(By.xpath(pole)).sendKeys(df.format(wartosc));				
				}

//Wypelnienie overload - pole tekstowe
		public void wypelnij (String pole, String tekst) throws InterruptedException{
			driver.findElement(By.xpath(pole)).clear();
			driver.findElement(By.xpath(pole)).sendKeys(tekst);
		}
		
//Wypelnienie pola typu combo 
		public void wypelnij_combo(String pole, String pozycja) throws InterruptedException{		
			driver.findElement(By.xpath(pole+"/div[1]")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath(pole+"/div[2]/div["+pozycja+"]")).click();
	
		}

//Kliknij
		public void kliknij (String pole) throws InterruptedException{	
			driver.findElement(By.xpath(pole)).click();
		}

//Kliknij overload - xpath wcześniej zdefiniowany + dalsza część xpath 
		public void kliknij (String pole, String ekstra) throws InterruptedException{	
			driver.findElement(By.xpath(pole+ekstra)).click();
	
		}
		
//Czekaj na obecność 
		public void czekajNaObecnosc(By locator) throws InterruptedException {
			Thread.sleep(500);
			this.wait.until(ExpectedConditions.presenceOfElementLocated(locator));
		}
		
//Sprawdzenie czy element istnieje 
		public boolean czyIstnieje(By xpath) {
			try {
				this.driver.findElement(xpath);
				return true;
			} catch (NoSuchElementException e) {
				return false;
			} catch (WebDriverException e) {
				return false;
			}
		}

//Formatowanie 
		public DecimalFormat getDf() {
			return df;
		}

//Znajdz zamówienie 
		public void znajdz_zam (String przedmiot) throws InterruptedException {
			kliknij(("/html[1]/body[1]/div[1]/div[3]/div[1]/h6[1]/div[1]/div[4]/table[1]/thead[1]/tr[1]/td[1]"));
			kliknij("/html[1]/body[1]/div[1]/div[3]/div[1]/h6[1]/div[1]/div[3]/div[1]");
			wypelnij("/html[1]/body[1]/div[1]/div[3]/div[1]/h6[1]/div[1]/div[4]/table[1]/thead[1]/tr[3]/td[8]/div[1]/input[1]", przedmiot);
			//wypelnij("/html[1]/body[1]/div[1]/div[3]/div[1]/h6[1]/div[1]/div[4]/table[1]/thead[1]/tr[3]/td[11]/div[1]/input[1]", wartosc);
			driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[3]/div[1]/h6[1]/div[1]/div[4]/table[1]/thead[1]/tr[3]/td[8]/div[1]/input[1]")).sendKeys(Keys.ENTER);
		}

//Zamien String na Double
		public Double str2d (String string){
			String temp = string.replace(" ", "").replace(",", ".");
			return Double.valueOf(temp);
			
		}

//Suma
		public String suma (String wartosc1, String wartosc2) {
			Double w1 = str2d(wartosc1)+str2d(wartosc2);
			return df.format(w1).replace(".", ",");
		}
		
//Iloraz
		public String iloraz(String wartosc1, String wartosc2) {
			Double w1 = str2d(wartosc2)/str2d(wartosc1);
			return df.format(w1).replace(".", ",");
		}
		
}
