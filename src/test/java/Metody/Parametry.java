package Metody;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class Parametry {
	private WebDriver driver;
	private String adres_url = "http://pzp1.doskomp.pl/pzp_dev";
	public static final String chrome = "driver/chromedriver.exe";
	
	private String przedmiot_zamowienia = "Przedmiot zamówienia do testów";
	private String kurs_euro = "4.2693"; 


public Parametry() {
			System.setProperty("webdriver.chrome.driver", chrome);
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			}
	
	public WebDriver getDriver() {
		return driver;
	}
	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	
	public String getAdres_url() {
		return adres_url;
	}
	public void setAdres_url(String adres_url) {
		this.adres_url = adres_url;
	}

	public String getPrzedmiot_zamowienia() {
		return przedmiot_zamowienia;
	}

	public void setPrzedmiot_zamowienia(String przedmiot_zamowienia) {
		this.przedmiot_zamowienia = przedmiot_zamowienia;
	}

	public String getKurs_euro() {
		return kurs_euro;
	}

	public void setKurs_euro(String kurs_euro) {
		this.kurs_euro = kurs_euro;
	}
	
}
