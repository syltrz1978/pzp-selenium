package Zamowienie;


import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import Metody.Mapowanie_xpath;
import Metody.Parametry;
import Metody.Podstawowe_metody;
import Metody.Zamowienie;


public class Zamowienie_dodaj {
		 private WebDriver driver;
		 private String baseUrl;
		 Podstawowe_metody podstawowe_metody;
		 WebDriverWait wait;
		 Mapowanie_xpath mapowanie = new Mapowanie_xpath();
		 private String przedmiot_zamowienia;
		 private String kurs_euro;
		 
		
	@Before
	 public void setUp() throws Exception {
		Parametry parametry = new Parametry();
		driver = parametry.getDriver();
		baseUrl = parametry.getAdres_url();
		podstawowe_metody = new Podstawowe_metody(driver);
		przedmiot_zamowienia=parametry.getPrzedmiot_zamowienia();
		kurs_euro=parametry.getKurs_euro();
	 }

	@Test
	 public void test() throws InterruptedException {

		driver.get(baseUrl);
		podstawowe_metody.logowanie();
		
		//Dodanie +wypelnienie zamowienia
		Zamowienie zam = new Zamowienie(driver);
		
		zam.plan_zamowienien();
		podstawowe_metody.kliknij(mapowanie.getDodaj_zam());
		zam.zamowienie("Przedmiot zamówienia " + podstawowe_metody.random_int(10000), "1", "1", "1", "16", "3", "Uzasadnienie do zamówienia", kurs_euro);

		//Weryfikacja - czy dodany 													
		podstawowe_metody.czekajNaObecnosc(By.xpath(mapowanie.getNaglowek()));
		podstawowe_metody.znajdz_zam(zam.getPozycje_map().get("przedmiot"));
		podstawowe_metody.czekajNaObecnosc(By.xpath(mapowanie.getNaglowek()));
		
		assertEquals(zam.getPozycje_map().get("przedmiot"), driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[3]/div[1]/h6[1]/div[1]/div[5]/table[1]/tbody[1]/tr[1]/td[8]")).getText());
		assertEquals(zam.getPozycje_map().get("wartosc"), driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[3]/div[1]/h6[1]/div[1]/div[5]/table[1]/tbody[1]/tr[1]/td[11]")).getText().replace(" ", ""));
	
		//przekazanie sumy kategorii do mapy
		zam.getPozycje_map().put("suma_kat", driver.findElement(By.xpath(mapowanie.getSuma_kat())).getText());
		System.out.println(zam.getPozycje_map().get("suma_kat"));
		
		//zamknięcie wiersza wyszukiwania
		podstawowe_metody.kliknij("/html[1]/body[1]/div[1]/div[3]/div[1]/h6[1]/div[1]/div[4]/table[1]/thead[1]/tr[3]/td[1]/span[1]");
		
		//dodanie zamówienia + wypełnienie - do weryfikacji sumy kategorii
		podstawowe_metody.kliknij(mapowanie.getDodaj_zam());
		zam.zamowienie(przedmiot_zamowienia, "1", "1", "1", "16", "3","Uzasadnienie do zamówienia", kurs_euro);
		
		//weryfikacja sumy kategorii 
		podstawowe_metody.czekajNaObecnosc(By.xpath(mapowanie.getNaglowek()));
		podstawowe_metody.znajdz_zam(zam.getPozycje_map().get("przedmiot"));
		podstawowe_metody.czekajNaObecnosc(By.xpath(mapowanie.getNaglowek()));	
		
		assertEquals(zam.getPozycje_map().get("przedmiot"), driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[3]/div[1]/h6[1]/div[1]/div[5]/table[1]/tbody[1]/tr[1]/td[8]")).getText());
		assertEquals(zam.getPozycje_map().get("wartosc"), driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[3]/div[1]/h6[1]/div[1]/div[5]/table[1]/tbody[1]/tr[1]/td[11]")).getText().replace(" ", ""));
		assertEquals(podstawowe_metody.suma(zam.getPozycje_map().get("suma_kat"),zam.getPozycje_map().get("wartosc")) , driver.findElement(By.xpath(mapowanie.getSuma_kat())).getText().replace(" ", ""));
		
	

		//@After
//		public void tearDown() throws Exception {
//		podstawowe_metody.wyloguj();
//		driver.quit();	
//		}

	}
	}


