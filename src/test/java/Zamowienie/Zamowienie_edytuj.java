package Zamowienie;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import Metody.Mapowanie_xpath;
import Metody.Parametry;
import Metody.Podstawowe_metody;
import Metody.Zamowienie;

public class Zamowienie_edytuj {
	 private WebDriver driver;
	 private String baseUrl;
	 Podstawowe_metody podstawowe_metody;
	 WebDriverWait wait;
	 Mapowanie_xpath mapowanie = new Mapowanie_xpath();
	 private String przedmiot_zamowienia;
	 private String kurs_euro;
	 
	 
	
@Before
public void setUp() throws Exception {
	Parametry parametry = new Parametry();
	driver = parametry.getDriver();
	baseUrl = parametry.getAdres_url();
	podstawowe_metody = new Podstawowe_metody(driver);
	przedmiot_zamowienia=parametry.getPrzedmiot_zamowienia();
	kurs_euro=parametry.getKurs_euro();
 }

@Test
public void test() throws InterruptedException {

	driver.get(baseUrl);
	podstawowe_metody.logowanie();
	
	
	Zamowienie zam = new Zamowienie(driver);
	podstawowe_metody.plan_zamowienien();;
	//czekaj na nagłówek
	podstawowe_metody.czekajNaObecnosc(By.xpath("/html[1]/body[1]/div[1]/div[3]/div[1]/h6[1]/div[1]/div[4]/table[1]/thead[1]/tr[1]/td[8]"));
	
	//znajdz zamowienie do edycji 
	podstawowe_metody.znajdz_zam(przedmiot_zamowienia);
	podstawowe_metody.czekajNaObecnosc(By.xpath("/html[1]/body[1]/div[1]/div[3]/div[1]/h6[1]/div[1]/div[4]/table[1]/thead[1]/tr[1]/td[8]"));
	podstawowe_metody.kliknij("/html[1]/body[1]/div[1]/div[3]/div[1]/h6[1]/div[1]/div[5]/table[1]/tbody[1]/tr[1]/td[1]/input[1]");
	
	//edycja zamówienia - ta sama nazwa inna kwota
	podstawowe_metody.kliknij(mapowanie.getEdytuj_zam());
	zam.zamowienie(przedmiot_zamowienia, "1", "1", "1", "16", "3", "Uzasadnienie do zamówienia",kurs_euro);
	
	//Weryfikacja - czy edytowany 
	//czekaj na nagłowek - zamknij okno do wyszukiwania - czekaj na nagłówek 
	podstawowe_metody.czekajNaObecnosc(By.xpath("/html[1]/body[1]/div[1]/div[3]/div[1]/h6[1]/div[1]/div[4]/table[1]/thead[1]/tr[1]/td[8]"));
	podstawowe_metody.kliknij(("/html[1]/body[1]/div[1]/div[3]/div[1]/h6[1]/div[1]/div[4]/table[1]/thead[1]/tr[3]/td[1]/span[1]"));
	podstawowe_metody.czekajNaObecnosc(By.xpath("/html[1]/body[1]/div[1]/div[3]/div[1]/h6[1]/div[1]/div[4]/table[1]/thead[1]/tr[1]/td[8]"));
	
	podstawowe_metody.kliknij(("/html[1]/body[1]/div[1]/div[3]/div[1]/h6[1]/div[1]/div[4]/table[1]/thead[1]/tr[1]/td[8]"));
	podstawowe_metody.kliknij("/html[1]/body[1]/div[1]/div[3]/div[1]/h6[1]/div[1]/div[1]/div[1]");
	podstawowe_metody.wypelnij("/html[1]/body[1]/div[1]/div[3]/div[1]/h6[1]/div[1]/div[4]/table[1]/thead[1]/tr[3]/td[8]/div[1]/input[1]", zam.getPozycje_map().get("przedmiot"));
	driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[3]/div[1]/h6[1]/div[1]/div[4]/table[1]/thead[1]/tr[3]/td[8]/div[1]/input[1]")).sendKeys(Keys.ENTER);
	
	Thread.sleep(1000);	
	assertEquals(zam.getPozycje_map().get("przedmiot"), driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[3]/div[1]/h6[1]/div[1]/div[5]/table[1]/tbody[1]/tr[1]/td[8]")).getText());
	assertEquals(zam.getPozycje_map().get("wartosc"), driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[3]/div[1]/h6[1]/div[1]/div[5]/table[1]/tbody[1]/tr[1]/td[11]")).getText().replace(" ", ""));

}
}
